#!/bin/bash

# Export environment variables in .env to current shell
source .env

echo " ▶ npm install"
npm install

if command -v "docker compose" &> /dev/null; then
    docker_command="docker compose"
elif command -v "docker-compose" &> /dev/null; then
    docker_command="docker-compose"
else
    echo "❌ Couldn't find \"docker compose\" or \"docker-compose\"."
    echo "- Install \"docker\" or \"docker-compose\""
    echo "Exiting setup script..."
fi

echo " ▶ docker compose down -v --remove-orphans"
$docker_command down -v --remove-orphans

echo " ▶ docker stop { $(docker ps -a | grep -Eo 'arin-smart-parking\S*') }"
docker stop $(docker ps -a | grep -Eo 'arin-smart-parking\S*')
wait

echo " ▶ docker rm -v { $(docker ps -a | grep -Eo 'arin-smart-parking\S*') }"
docker rm -v $(docker ps -a | grep -Eo 'arin-smart-parking\S*')
wait

echo " ▶ docker compose up --build -d"
$docker_command -p arin-smart-parking-app up --build -d

# Check if localhost:3000 is active
retry=0
while true; do
    echo "Retrying..."
    if (( $retry > 10)); then
        echo "🐌 Script stuck at \"docker compose up\". Exiting ..."
        exit 1
    fi
    # If active then proceed forward in script. Else exit script.
    if ss -tulpn | grep $PORT; then
        echo "🌐 Server started at port 3000"
        break
    fi

    let "retry++"
    sleep 5
done

# Server Online
#-----------------------------------------------------------------------------
set +e 

app_container="arin-smart-parking-app-app"

echo "Executing commands inside app container : $app_container"
printf "%s\n" "---------------------------------------------------------"

printf "%s\n" " ▶ Inside DB docker -> Deleting and Recreating Database"
docker exec $app_container sh -c "npx sequelize-cli db:drop && npx sequelize-cli db:create"

printf "%s\n" " ▶ Inside DB docker -> Running Migration and Seeding"
docker exec $app_container sh -c "npx sequelize-cli db:migrate && npx sequelize-cli db:seed:all"

echo "🎉🎉 You are all set up 🎉🎉"
echo "Visit http://localhost:3000"
echo "Opening http://localhost:3000/lot for you..."

xdg-open "http://localhost:3000/lot"