# Smart Parking Application

## Installation

1. In the project directory run the setup script -
```
>> ./project-setup.sh
```
The above script would set up your environment and launch the application.

⚠️ **IMPORTANT -** Run the above command just one during initial setup.

To run the application after the `project-setup` script has been run. You can do -
```
>> docker compose up
```

- The server will be running on `localhost:3000`


## Schema

![Schema Diagram](./database_schema_diagram.png)
