const { DataTypes } = require('sequelize');
const sequelize = require('./db');

const Lot = sequelize.define('lots', {
    name: {
        type: DataTypes.STRING,
        primaryKey: true,
        validate: {
            is: /^P[0-9]+$/i
        }
    },
    coordinate: {
        type: DataTypes.GEOMETRY('POINT', 4326),
        allowNull: false,
    },
    gatewayId: {
        type: DataTypes.STRING(20),
        allowNull: false,
        unique: true,
    }
});

const Bay = sequelize.define('bays', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            is: /^B[0-9]+$/i
        }
    },
    position: {
        type: DataTypes.GEOMETRY('POINT', 4326),
        allowNull: false,
    },
    lotName: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
            model: 'lots',
            key: 'name'
        }
    },
    sensorId: {
        type: DataTypes.STRING(20),
        allowNull: false,
        unique: true,
    },
    occupied: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
    },
    lastMessageTime: {
        type: DataTypes.DATE,
        allowNull: true,
    }
});

Lot.hasMany(Bay, { foreignKey: 'lotName', onDelete: 'CASCADE' });
Bay.belongsTo(Lot, { foreignKey: 'lotName' });


module.exports = { Lot, Bay };
