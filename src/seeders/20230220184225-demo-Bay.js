const { fn } = require('sequelize');
const { v4: uuidv4 } = require('uuid');

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert('bays',
            [
                {
                    name: 'B1', position: fn('ST_SetSRID', fn('ST_MakePoint', 40.431389, -80.050556), 4326),
                    lotName: 'P1', sensorId: uuidv4()
                },
                {
                    name: 'B2', position: fn('ST_SetSRID', fn('ST_MakePoint', 40.431389, -80.050556), 4326),
                    lotName: 'P1', sensorId: uuidv4()
                },
                {
                    name: 'B3', position: fn('ST_SetSRID', fn('ST_MakePoint', 40.431389, -80.050556), 4326),
                    lotName: 'P1', sensorId: uuidv4()
                },
                {
                    name: 'B4', position: fn('ST_SetSRID', fn('ST_MakePoint', 100.23400, 180.050556), 4326),
                    lotName: 'P2', sensorId: uuidv4()
                },
                {
                    name: 'B5', position: fn('ST_SetSRID', fn('ST_MakePoint', 100.23400, 180.050556), 4326),
                    lotName: 'P2', sensorId: uuidv4()
                },
                {
                    name: 'B6', position: fn('ST_SetSRID', fn('ST_MakePoint', 213.13819, 10.050556), 4326),
                    lotName: 'P3', sensorId: uuidv4()
                },
                {
                    name: 'B7', position: fn('ST_SetSRID', fn('ST_MakePoint', 213.13819, 10.050556), 4326),
                    lotName: 'P3', sensorId: uuidv4()
                }
            ],
            {});
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('lots', null, {});
    }
};
