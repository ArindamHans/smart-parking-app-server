const { fn } = require('sequelize');
const { v4: uuidv4 } = require('uuid');

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert('lots',
            [
                { name: 'P1', coordinate: fn('ST_SetSRID', fn('ST_MakePoint', 40.431389, -80.050556), 4326),
                    gatewayId: uuidv4() },
                { name: 'P2', coordinate: fn('ST_SetSRID', fn('ST_MakePoint', 100.23400, 180.050556), 4326),
                    gatewayId: uuidv4() },
                { name: 'P3', coordinate: fn('ST_SetSRID', fn('ST_MakePoint', 213.13819, 10.050556), 4326),
                    gatewayId: uuidv4() },
            ],
            {});
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('lots', null, {});
    }
};
