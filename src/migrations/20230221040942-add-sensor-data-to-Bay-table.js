'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('bays', 'occupied', {
            type: Sequelize.BOOLEAN,
            allowNull: true
        });
        await queryInterface.addColumn('bays', 'lastMessageTime', {
            type: Sequelize.TIME,
            allowNull: true
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('bays', 'occupied');
        await queryInterface.removeColumn('bays', 'lastMessageTime');
    }
};
