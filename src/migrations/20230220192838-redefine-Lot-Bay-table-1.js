'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('bays');
        await queryInterface.dropTable('lots');
        await queryInterface.createTable('lots', {
            name: {
                type: Sequelize.STRING,
                primaryKey: true,
                validate: {
                    is: /^P[0-9]+$/i,
                }
            },
            coordinate: {
                type: Sequelize.GEOMETRY('POINT', 4326),
                allowNull: false
            },
            gatewayId: {
                type: Sequelize.STRING(40),
                allowNull: false,
                unique: true,
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.fn('NOW')
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.fn('NOW')
            }
        });

        await queryInterface.createTable('bays', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false,
                validate: {
                    is: /^B[0-9]+$/i
                }
            },
            lotName: {
                type: Sequelize.STRING,
                allowNull: false,
                references: {
                    model: 'lots',
                    key: 'name'
                }
            },
            position: {
                type: Sequelize.GEOMETRY('POINT', 4326),
                allowNull: false
            },
            sensorId: {
                type: Sequelize.STRING(40),
                allowNull: false,
                unique: true,
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.fn('NOW')
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
                defaultValue: Sequelize.fn('NOW')
            }
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('bays');
        await queryInterface.dropTable('lots');
    }
};
