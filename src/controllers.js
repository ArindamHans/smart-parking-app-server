
const { Lot, Bay } = require('./models');

// Fetch list of all parking lots
async function getAllLots(req, res, next) {
    try {
        const lots = await Lot.findAll({
            include: [{ model: Bay }],
        });
        res.json(lots);
    } catch (error) {
        console.log(error);
    }
}

// Fetch lot details along with list of all bays within that lot
async function getLotDetails(req, res, next) {
    try {
        const lot = await Lot.findOne({
            where: { name: req.params.lot_name },
            include: [{ model: Bay }],
        });
        if (!lot) {
            return res.status(404).send(`Lot ${lot_name} not found`);
        }
        res.json(lot);
    } catch (error) {
        console.log(error);
    }
}

// Fetch bay details by global name
async function getBayDetails(req, res, next) {
    try {
        const bay = await Bay.findOne({
            where: { name: req.params.bay_name },
            include: [{ model: Lot, where: { name: req.params.lot_name } }],
        });
        if (!bay) {
            return res.status(404).send(`Bay ${lot_name}-${bay_name} not found`);
        }
        res.json(bay);
    } catch (error) {
        console.log(error);
    }
}

// Insert a new data point corresponding to a sensor
async function addSensorData(req, res, next) {
    try {
        console.log('⧈ addSensorData() | req = ', req.params);
    } catch (error) {
        next(error);
    }
}

module.exports = { getAllLots, getLotDetails, getBayDetails, addSensorData };
