const mqtt = require('mqtt');
const { Lot, Bay } = require('./models');

// Connect to MQTT broker
const mqttClient = mqtt.connect('mqtt://broker.emqx.io/1883', {
    clientId: 'mqttjs-server-1234',
    encoding: 'utf-8'
});
const mqttTopic = 'sensor/+/data'

// Subscribe to topic after connecting to broker
mqttClient.on('connect', () => {
    mqttClient.subscribe(mqttTopic, (error) => {
        console.log(error);
    });
});

// Handle messages. Insert into database
mqttClient.on('message', async (topic, message) => {
    const messageJSON = JSON.parse(message.toString());
    const recievedSensorId = topic.match(/sensor\/(.*?)\//)[1];
    console.log(messageJSON, recievedSensorId);

    try {
        if (message) {
            const result = await Bay.update(
                { occupied: messageJSON.occupied, lastMessageTime: messageJSON.timestamp },
                { where: { sensorId: recievedSensorId }, returning: true, plain: true }
            );
        }
    } catch (error) {
        console.log(error);
    }
})


module.exports = mqttClient;
