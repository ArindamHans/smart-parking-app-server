const Sequelize = require('sequelize');

const sequelize = new Sequelize({
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    dialect: 'postgres'
});


// Check connection to DB
(async () => {
    try {
        await sequelize.authenticate();
        console.log('⧈ Connection has been established successfully.');
    } catch (error) {
        console.error('⧈ Unable to connect to the database:', error);
    }
})();

module.exports = sequelize;
