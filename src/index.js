const express = require('express');
const dotenv = require('dotenv').config();
const routes = require('./routes');
const mqttClient = require('./mqtt-handler');

const app = express();

app.use(routes);

// Start the server
app.listen(process.env.PORT, () => {
  console.log(`Server listening on port ${process.env.PORT}`);
});
