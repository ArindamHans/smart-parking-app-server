const express = require('express');
const router = express.Router();

const controller = require('./controllers');

// GET ------------------------------------------
router.get('/lot', controller.getAllLots);
router.get('/lot/:lot_name', controller.getLotDetails);
router.get('/bay/:lot_name/:bay_name', controller.getBayDetails);

// POST -----------------------------------------
// router.post('/data', controller.addSensorData);

module.exports = router;
