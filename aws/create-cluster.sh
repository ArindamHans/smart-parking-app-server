#!/bin/bash
KEY_PAIR=smart-parking-app-cluster
ecs-cli up \
  --keypair $KEY_PAIR  \
  --capability-iam \
  --size 2 \
  --instance-type t3.medium \
  --tags project=smart-parking-app-cluster,owner=arin \
  --cluster-config smart-parking-app \
  --ecs-profile smart-parking-app \
  --force
