#!/bin/bash

ecs-cli compose --project-name smart-parking-app \
    --file ./docker-compose.ecs.yml \
    --debug service up  \
    --deployment-max-percent 100 --deployment-min-healthy-percent 0 \
    --region ap-southeast-1 --ecs-profile smart-parking-app \
    --cluster-config smart-parking-app \
    --timeout 15
